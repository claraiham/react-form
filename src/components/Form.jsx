import { useState } from 'react';
import './Form.css';

//onSubmit={this.handleSubmit}

function Form() {

  //useref ?

    const [lastName, setLastName] = useState('');
    const [firstName, setFirstName] = useState('');
    const [age, setAge] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorAge, setErrorAge] = useState('');
    const [errorEmail, setErrorEmail] = useState('');

    


    const onSubmit = (event) => {
      event.preventDefault();

      //vérifier l'âge
      if (age < 18) setErrorAge("Vous n'avez pas l'âge recquis");


      // vérifier l'e-mail
      function isEmailValid(email){
        let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  
        if ( !email.match(re)){
          setErrorEmail("Adresse email invalide")
        }
  
      }

      isEmailValid(email);

      


      const data = {
        lastName : lastName ,
        firstName : firstName,
        age : age,
        email : email,
        password : password,
      }

      console.log(data);

    };



  return (
    <form onSubmit={onSubmit}>
      <label>
        Nom :
        <input value={lastName} onChange={(e) => setLastName(e.target.value)} type="text"/>
      </label>
      <label>
        Prénom :
        <input value={firstName} onChange={(e) => setFirstName(e.target.value)} type="text"/>
      </label>
      <label>
        Age :
        <input value={age} onChange={(e) => setAge(e.target.value)} type="number"/>
        {errorAge}
      </label>
      <label>
        E-mail :
        <input value={email} onChange={(e) => setEmail(e.target.value)} type="email"/>
        {errorEmail}
      </label>
      <label>
        Mot de passe :
        <input value={password} onChange={(e) => setPassword(e.target.value)} type="password"/>
      </label>
      <button onClick={onSubmit}>Envoyer</button>
    </form>
  )
}

export default Form